require 'spec_helper'

describe 'math::exp' do
  it '2^3=8' do
    is_expected.to run.with_params(2, 3).and_return(8)
  end
  it '2^4=16' do
    is_expected.to run.with_params(2, 4).and_return(16)
  end
  it '2^8=16' do
    is_expected.to run.with_params(2, 8).and_return(256)
  end
  it '2^1=2' do
    is_expected.to run.with_params(2, 1).and_return(2)
  end
  it '2^0=1' do
    is_expected.to run.with_params(2, 0).and_return(2)
  end
  it '0^0=0' do
    is_expected.to run.with_params(0, 0).and_return(0)
  end
  it '1^0=1' do
    is_expected.to run.with_params(1, 0).and_return(1)
  end
  it '0^1=0' do
    is_expected.to run.with_params(0, 1).and_return(0)
  end
  it '2^63=' do
    is_expected.to run.with_params(2, 63).and_raise_error(ArgumentError)
  end
  it '3^63=' do
    is_expected.to run.with_params(3, 61).and_raise_error(Puppet::ParseError, %r{max})
  end
end
