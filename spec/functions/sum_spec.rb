require 'spec_helper'

describe 'math::sum' do
  it 'with array' do
    is_expected.to run.with_params([2, 2, 2, 2]).and_return(8)
  end

  it 'without array' do
    is_expected.to run.with_params(2, 2, 2, 2).and_return(8)
  end

  it 'single value' do
    is_expected.to run.with_params(2).and_return(2)
  end

  it 'no value' do
    is_expected.to run.and_raise_error(Puppet::Pops::Types::TypeAssertionError)
  end

  it 'with array of array' do
    is_expected.to run.with_params([2, 2, 2, 2, [3, 3, 3]]).and_return(17)
  end
end
