require 'spec_helper'

describe 'math::square' do
  it '2^2=4' do
    is_expected.to run.with_params(2).and_return(4)
  end
end
