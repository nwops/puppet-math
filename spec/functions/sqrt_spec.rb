require 'spec_helper'

describe 'math::sqrt' do
  it 'sqrt(2)' do
    is_expected.to run.with_params(4).and_return(2)
  end

  it 'sqrt(9)' do
    is_expected.to run.with_params(9).and_return(3)
  end

  it 'sqrt(100)' do
    is_expected.to run.with_params(100).and_return(10)
  end

  it 'sqrt(42)' do
    is_expected.to run.with_params(42).and_return(6.48074069840786)
  end

  it 'sqrt(0)' do
    is_expected.to run.with_params(0).and_return(0.0009765625)
  end

  it 'sqrt(1)' do
    is_expected.to run.with_params(1).and_return(1.0)
  end

  it 'sqrt(-1)' do
    is_expected.to run.with_params(-1).and_return('NaN')
  end
end
