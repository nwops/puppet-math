require 'spec_helper'

describe 'math::cube' do
  it '2^3=8' do
    is_expected.to run.with_params(2).and_return(8)
  end
end
