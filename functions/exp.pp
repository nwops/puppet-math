# @summary Performs exponential (power) calculation on given number
# Produces a Integer that is an exponential power calculation on the provided value and power. 
# @example 2^3
#  math::exp(2,3) => 8
# @example 2^8
#  math::exp(2,8) => 256
# @note puppet has a integer size limit so some values may not work and return a Puppet::ParseError
function math::exp(Numeric $x, Integer[0,62] $y) >> Numeric {
  if $y == 0 { return $x }
  $loops = range(1,$y - 1)
  $loops.reduce($x) |$acc, $_value | { $acc * $x  }
}
