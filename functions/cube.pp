# @summary Cubes a number
# @example 2
#  math::cube(2) => 8
# @example 3
#  math::cube(3) => 27
# @note puppet has a integer size limit so some values may not work and return a Puppet::ParseError
function math::cube(Numeric $value) >> Numeric {
    $value * $value * $value
}
