# @summary rounds to the nearset decimal
function math::round(Numeric $x) >> Numeric {
  # needs implementation with precision 
  $x.round
}
