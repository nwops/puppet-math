function math::constants(String $name) >> Any {
  {
    'golden_ratio' => 1.618034
  }.dig($name)
}
