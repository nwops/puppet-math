# @summary Calculate the fibonacci sequence for the given number
# @note The input value maxes out at 27 because it takes too long to calclulate
# @note Example of how to perform this calculation
#   https://www.wikihow.com/Calculate-the-Fibonacci-Sequence
# 
function math::fibonacci(Numeric $n, Enum[table,binet] $method = 'table') >> Integer {
  call("math::fibonacci::${method}", $n)
}
