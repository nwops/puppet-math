# @summary Produces a sum of all the numbers
# Produces a Integer that is sum of all the numbers provided.  Can take arrays or multiple values.
# @example with array
#  math::sum([2,2,2]) => 6
# @example without array
#  math::sum(2,2,2) => 6
# @note puppet has a integer size limit so some values may not work and return a Puppet::ParseError
function math::sum(Variant[Numeric, Array[Numeric], Tuple] *$value ) >> Numeric {
  $value.flatten.reduce | Numeric $acc, Numeric $v| { $acc + $v }
}
