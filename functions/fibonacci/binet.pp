# @summary Calculate the fibonacci sequence for the given number using binet method
# @note The input value maxes out at 27 because it takes too long to calclulate
# @note Example of how to perform this calculation
#   https://www.wikihow.com/Calculate-the-Fibonacci-Sequence
# 
function math::fibonacci::binet(Numeric $n) >> Numeric {
  $ratio = math::constants(golden_ratio)
  $a = math::exp($ratio, $n)
  $b = (math::exp(1 - $ratio, $n))
  $c = $a - $b
  $d = $c / math::sqrt($n)
  $d.round
}
