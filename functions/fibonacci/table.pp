# @summary Calculate the fibonacci sequence for the given number using table method
# @note The input value maxes out at 27 because it takes too long to calclulate
# @note Example of how to perform this calculation
#   https://www.wikihow.com/Calculate-the-Fibonacci-Sequence
# 
function math::fibonacci::table(Numeric $n) >> Numeric {
  if $n <= 1 {
    return $n
  }
  ( math::fibonacci::table( $n - 1 ) + math::fibonacci::table( $n - 2 ) )
}
