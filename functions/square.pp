# @summary Squares a number
# @example 2
#  math::square(2) => 4
# @example 3
#  math::square(3) => 9
# @note puppet has a integer size limit so some values may not work and return a Puppet::ParseError
function math::square(Numeric $value) >> Numeric {
  $value * $value
}
