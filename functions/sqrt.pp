# @summary returns the square root of the numeric value $x.
# @note This implementation uses Newton's method.
# @example Square root of 9
#   math::sqrt(9) => 3
function math::sqrt(Numeric $x ) >> Numeric {
  if $x < 0 {
    return 'NaN'
  }
  # let initial guess be 1.0
  $value = 10.reduce(1.0) |$z,$i| {
    $z - ($z*$z - $x) / (2*$z) # magic
  }
  $value
}
