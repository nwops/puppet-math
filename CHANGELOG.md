# Changelog
All notable changes to this project will be documented in this file.

## Release 0.2.0
 * Adds fibonacci functions
 * Adds round function
 * Adds sum function
 * Adds sqrt function - thanks @helindbe 
 
## Release 0.1.0

**Features**

**Bugfixes**

**Known Issues**
